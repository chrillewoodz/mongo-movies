// Required plugins
var gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss    = require('gulp-minify-css'),
    jshint       = require('gulp-jshint'),
    uglify       = require('gulp-uglify'),
    imagemin     = require('gulp-imagemin'),
    rename       = require('gulp-rename'),
    concat       = require('gulp-concat'),
    notify       = require('gulp-notify'),
    cache        = require('gulp-cache'),
    htmlmin      = require('gulp-htmlmin'),
    gulpif       = require('gulp-if'),
    del          = require('del');

// Flags used for conditional logic
var flags = {
  build: false
}

// Minify our pages HTML
gulp.task('views', function() {
  return gulp.src('../app/public/views/pages/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulpif(flags.build, gulp.dest('../app/dist/views/pages')));
});

// Minify our partial templates HTML
gulp.task('partials', function() {
  return gulp.src('../app/public/views/partials/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulpif(flags.build, gulp.dest('../app/dist/views/partials')));
});

// Compile our bower scripts into a dist vendor file
gulp.task('vendor-scripts', function() {
  return gulp.src(['../app/public/bower_components/angular/angular.min.js',
                   '../app/public/bower_components/angular-route/angular-route.min.js',
                   '../app/public/bower_components/angular-animate/angular-animate.min.js',
                   '../app/public/bower_components/jquery/jquery.min.js'])
    .pipe(concat('vendor.js'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('../app/public/assets'))
    .pipe(gulpif(flags.build, gulp.dest('../app/dist/assets/scripts')));
});

// Compile our vendor styles into a dist vendor file
gulp.task('vendor-styles', function() {
  return gulp.src(['../app/public/bower_components/bootstrap/dist/css/bootstrap.min.css'])
    .pipe(concat('vendor.css'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulpif(flags.build, gulp.dest('../app/dist/assets/styles')));
});

// Copy over our server file
gulp.task('server', function() {
  return gulp.src('../app/server.js')
    .pipe(gulpif(flags.build, gulp.dest('../app/dist')));
});

// Copy our models 
gulp.task('models', function() {
  return gulp.src('../app/models/*.js')
    .pipe(gulpif(flags.build, gulp.dest('../app/dist')));
});

// Compile our SCSS into minified CSS
gulp.task('styles', function() {
  return gulp.src('../app/public/assets/styles/style.scss')
    .pipe(sass({ style: 'expanded' }))
    .on('error', function handleError(err) {
      console.error(err.toString());
      this.emit('end');
    })
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(gulpif(flags.build, gulp.dest('../app/dist/assets/styles')))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest('../app/public/assets/styles'))
    .pipe(gulpif(flags.build, gulp.dest('../app/dist/assets/styles')))
    .pipe(notify({ message: 'Finished compiling SCSS' }));
});

// Clean our concatenated dev main.js file
// to prevent duplicated code
gulp.task('script-clean', function(cb) {
  del(['../app/public/assets/scripts/main.js'], { force: true }, cb);
});

// Concat and compile our JS into a minified dist file
gulp.task('scripts', ['script-clean'], function() {
  return gulp.src(['../app/public/assets/scripts/modules.js',
                   '../app/public/assets/scripts/routes/express.js',
                   '../app/public/assets/scripts/routes/angular.js',
                   '../app/public/assets/scripts/**/*.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('../app/public/assets/scripts'))
    .pipe(gulpif(flags.build, gulp.dest('../app/dist/assets/scripts')))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulpif(flags.build, gulp.dest('../app/dist/assets/scripts')))
    .pipe(notify({ message: 'Finished compiling scripts' }));
});

// Compress our images
gulp.task('images', function() {
  return gulp.src('../app/public/assets/images/**/*.js')
    .pipe(cache(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true })))
    .pipe(gulpif(flags.build, gulp.dest('../app/dist/assets/images')))
    .pipe(notify({ message: 'Finished compiling images' }));
});

// Clean/empty our dist folder
gulp.task('clean', function(cb) {
  del(['../app/dist'], { force: true }, cb);
});

// Build a project dist folder
gulp.task('build', ['clean'], function() {
  flags.build = true;
  gulp.start('views', 'partials', 'vendor-scripts', 'vendor-styles', 'server', 'styles', 'scripts', 'images');
});

// Run some default tasks when running 'gulp'
gulp.task('default', function() {
  gulp.start('watch');
});

// Watch our files for changes
gulp.task('watch', function() {
  gulp.watch('../app/public/assets/styles/**/*.scss', ['styles']);
  gulp.watch('../app/models/*.js', ['models']);
  gulp.watch('../app/public/assets/scripts/**/*.js', ['scripts']);
  gulp.watch('../app/public/assets/images/**/*', ['images']);
});