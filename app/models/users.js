var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
  firstname: String,
  lastname: String,
  useDisplayName: {
    type: Boolean,
    unique: true
  },
  displayName: String,
  email: {
    type: String,
    unique: true
  },
  password: String
});

mongoose.model('User', UserSchema);


