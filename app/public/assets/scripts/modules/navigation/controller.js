'use strict';

navigation.controller('NavCtrl', ['$scope', function($scope) {
  
  $scope.menu = [
    {label: 'Test1', target: 'test1'},
    {label: 'Test2', target: 'test2'},
    {label: 'Test3', target: 'test3'},
    {label: 'Test4', target: 'test4'},
    {label: 'Test5', target: 'test5'},
    {label: 'Test6', target: 'test6'}
  ];
}]);