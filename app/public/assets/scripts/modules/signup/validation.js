'use strict';

signup.factory('validateSignup', ['$q', function($q) {
  
  return function(newUser) {
    
    var deferred = $q.defer();
    
    for (var prop in newUser) {
      if (newUser[prop] === '') {
        deferred.resolve('All fields must be filled in');
      }
    }
    
    if (newUser.email === undefined) {
      deferred.resolve('Invalid email format');
    }
    else if (newUser.password.length < 8) {
      deferred.resolve('The password is too short');
    }
    else if (newUser.password != newUser.confirmedPass) {
      deferred.resolve('The passwords do not match');
    }
    else {
      deferred.resolve(true);
    }
    
    deferred.reject('Something went wrong');
    
    return deferred.promise;
  }
}]);