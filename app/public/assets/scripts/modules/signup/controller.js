'use strict';

signup.controller('SignupCtrl', ['$scope', 'validateSignup', function($scope, validateSignup) {
  
  $scope.newUser = {
    fname: '',
    lname: '',
    useDisplayName: false,
    email: '',
    password: '',
    confirmedPass: ''
  };
  
  
  
  $scope.completeSignup = function() {
    
    var validate = validateSignup($scope.newUser);
    
    $scope.valError = '';
    
    validate.then(function(status) {
      
      if (status === true) {
        console.log('We are good to go!');
      }
      else {
        $scope.valError = status;
      }
    }, function(err) {
      console.log(err);
    });
    
  };
  
}]);