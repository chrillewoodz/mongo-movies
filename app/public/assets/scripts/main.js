'use strict';

// Core module
var core = angular.module('core', [
      'ngRoute',
      'ngAnimate',
      'auth',
      'navigation',
      'signup'
    ]);

// Sub modules
// Make sure to inject these into the core module
var auth         = angular.module('auth', []),
    navigation   = angular.module('navigation', []),
    signup       = angular.module('signup', []); 
core.config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
    $routeProvider.
      when('/', {
        templateUrl: ''
      }).
      when('/signup', {
        templateUrl: 'views/pages/signup.html',
        controller: 'SignupCtrl'
      }).
      when('/signin', {
        templateUrl: 'views/pages/signin.html',
        controller: 'SigninCtrl'
      }).
      otherwise({
        redirectTo: '/'
      });
    $locationProvider.html5Mode(true);
}]);
'use strict';

auth.controller('AuthCtrl', ['$scope', 'userAuth', function($scope, userAuth) {
  
}]);
'use strict';

auth.factory('userAuth', ['$q', function($q) {
  
  return {
    createUser: function(newUser) {
      return;
    },
    authUser: function(userInfo) {
      return;
    },
    userSignout: function() {
      return;
    }
  }
}]);
'use strict';

core.controller('MainCtrl', ['$scope', function($scope) {
  
  $scope.paths = {
    menu: 'views/partials/menu.html'
  };
  
}]);
'use strict';

navigation.controller('NavCtrl', ['$scope', function($scope) {
  
  $scope.menu = [
    {label: 'Test1', target: 'test1'},
    {label: 'Test2', target: 'test2'},
    {label: 'Test3', target: 'test3'},
    {label: 'Test4', target: 'test4'},
    {label: 'Test5', target: 'test5'},
    {label: 'Test6', target: 'test6'}
  ];
}]);
'use strict';

signup.controller('SigninCtrl', ['$scope', 'validateSignin', function($scope, validateSignin) {
  
}]);
'use strict';

signup.factory('validateSignin', ['$q', function($q) {
  
  return function(userInfo) {
    
    var deferred = $q.defer();

    
    return deferred.promise;
  }
}]);
'use strict';

signup.controller('SignupCtrl', ['$scope', 'validateSignup', function($scope, validateSignup) {
  
  $scope.newUser = {
    fname: '',
    lname: '',
    useDisplayName: false,
    email: '',
    password: '',
    confirmedPass: ''
  };
  
  
  
  $scope.completeSignup = function() {
    
    var validate = validateSignup($scope.newUser);
    
    $scope.valError = '';
    
    validate.then(function(status) {
      
      if (status === true) {
        console.log('We are good to go!');
      }
      else {
        $scope.valError = status;
      }
    }, function(err) {
      console.log(err);
    });
    
  };
  
}]);
'use strict';

signup.factory('validateSignup', ['$q', function($q) {
  
  return function(newUser) {
    
    var deferred = $q.defer();
    
    for (var prop in newUser) {
      if (newUser[prop] === '') {
        deferred.resolve('All fields must be filled in');
      }
    }
    
    if (newUser.email === undefined) {
      deferred.resolve('Invalid email format');
    }
    else if (newUser.password.length < 8) {
      deferred.resolve('The password is too short');
    }
    else if (newUser.password != newUser.confirmedPass) {
      deferred.resolve('The passwords do not match');
    }
    else {
      deferred.resolve(true);
    }
    
    deferred.reject('Something went wrong');
    
    return deferred.promise;
  }
}]);