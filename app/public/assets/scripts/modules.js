'use strict';

// Core module
var core = angular.module('core', [
      'ngRoute',
      'ngAnimate',
      'auth',
      'navigation',
      'signup'
    ]);

// Sub modules
// Make sure to inject these into the core module
var auth         = angular.module('auth', []),
    navigation   = angular.module('navigation', []),
    signup       = angular.module('signup', []); 