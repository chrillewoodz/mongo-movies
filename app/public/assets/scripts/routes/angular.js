core.config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
    $routeProvider.
      when('/', {
        templateUrl: ''
      }).
      when('/signup', {
        templateUrl: 'views/pages/signup.html',
        controller: 'SignupCtrl'
      }).
      when('/signin', {
        templateUrl: 'views/pages/signin.html',
        controller: 'SigninCtrl'
      }).
      otherwise({
        redirectTo: '/'
      });
    $locationProvider.html5Mode(true);
}]);