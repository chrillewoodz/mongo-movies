// Required modules
var http      = require('http'),
    express   = require('express'),
    app       = express(),
    path      = require('path'),
    mongoose  = require('mongoose'),
    morgan    = require('morgan'),
    mongoConnect = require('connect-mongo');

var db = require('./models/users.js');
var connect = mongoose.connect('mongodb://localhost/testapp');

// Define what folder our app should use 
app.use(express.static(__dirname + '/public'));

// Define our view engine
app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');

// Define where to look for our views
app.set('views', __dirname + '/public/views');

// Log all requests
app.use(morgan('dev'));

require('./express.js')(app);

// Define port to listen at
app.listen(8080);
console.log('Listening on port 8080');
